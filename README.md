Carerix Form 8.x-1.x
--------------------

### About

The primary features of this module:

- A standard out of the box form entity (open_application) to use in your Drupal
- Interfaces configurable Carerix form entities
- Synchronize Carerix data nodes into the Drupal system to allow mapped data submissions
- A Display suite field to attach a Carerix form entity to your node types.

### Goals

This module aims to update data into the designated Carerix platform via the Carerix API Client module

### Installation

@todo